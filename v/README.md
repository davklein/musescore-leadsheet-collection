# v

* [Va' Hete'ren Här'å?](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/va-hete-ren-haer-a.mscx)
* [Va Pensiero](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/va-pensiero.mscx) (Guiseppe Verdi)
* [Vagabond King Waltz](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vagabond-king-waltz.mscx)
* [Vagamente](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vagamente.mscx)
* [Valencia](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/valencia.mscx) (Jose Padilla)
* [Vals I Lekstugan](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vals-i-lekstugan.mscx)
* [Valsa-Rancho](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/valsa-rancho.mscx)
* [Valse de Manchourie](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/valse-de-manchourie.mscx)
* [Valse Française](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/valse-francaise.mscx)
* [Valse Hot](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/valse-hot.mscx) (Sonny Rollins)
* [Valse N°2](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/valse-ndeg2.mscx) (D.Chostakovitch)
* [Valse](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/valse.mscx)
* [ValseBouquet de pensées](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/valsebouquet-de-pensees.mscx) (Anonyme)
* [Valsinha](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/valsinha.mscx)
* [Vanha Cowboy](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vanha-cowboy.mscx)
* [Var Blev Ni Av](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/var-blev-ni-av.mscx)
* [Variable](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/variable.mscx) (Michel PLATRE)
* [Varshaver Freylekhs](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/varshaver-freylekhs.mscx)
* [Varsity Drag](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/varsity-drag.mscx) (DeSylva, Brown & Ray Henderson)
* [Vat You Goed En Trek](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vat-you-goed-en-trek.mscx)
* [Vater unser, der du bist im Himmel](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vater-unser-der-du-bist-im-himmel.mscx) (Arr.: Franz Adam)
* [Vaya Con Dios](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vaya-con-dios.mscx) (Larry Russell, Inez James and Buddy Pepper)
* [Vaya Con Dios](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vaya-con-dios2.mscx)
* [Veinte Años](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/veinte-anos.mscx) (Maria Teresa Veras)
* [Vem tar hand om hösten?](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vem-tar-hand-om-hoesten.mscx) (Lars Göransson)
* [Vem Vet [Who Knows]](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vem-vet-who-knows.mscx) (Lisa Ekdahl)
* [Veni, Creator Spiritus](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/veni-creator-spiritus.mscx)
* [Veni Jesu](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/veni-jesu.mscx)
* [Venne Sangen [Priends Song]](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/venne-sangen-priends-song.mscx)
* [Venus In Blue Jeans](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/venus-in-blue-jeans.mscx) (Jack Keller and Howard Greenfield)
* [Venus](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/venus.mscx) (Oh Venus ,   make my dreams   come   true .)
* [Venus](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/venus2.mscx) (Ed Marshall)
* [Venus](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/venus3.mscx) (R Van Leeuwen)
* [Venutian Rhythm](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/venutian-rhythm.mscx)
* [Ver In Die Wereld [Kitty]](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/ver-in-die-wereld-kitty.mscx)
* [Vera](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vera.mscx)
* [Verde](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/verde.mscx) (Ricky King)
* [Very Early](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/very-early.mscx) (Bill Evans)
* [Viaggio Nella Vita](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/viaggio-nella-vita.mscx)
* [Video Games](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/video-games.mscx) (Words & Music by Elizabeth Grant & Justin Parker)
* [Video killed the radio star](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/video-killed-the-radio-star.mscx) (Buggles)
* [Vieni Sul Mar](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vieni-sul-mar.mscx)
* [Viennese Waltz](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/viennese-waltz.mscx)
* [Vies klein varkentje](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vies-klein-varkentje.mscx)
* [Vil Du Ride](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vil-du-ride.mscx)
* [Vilia](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vilia.mscx) (Franz Lehar)
* [Vilia](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vilia2.mscx) (Music by Franz Lehar)
* [Villancico III](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/villancico-iii.mscx)
* [Vincent](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vincent.mscx) (Don McLean)
* [Vincent](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vincent2.mscx) (Don McLean)
* [Vinegar & Salt](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vinegar-salt.mscx) (Hooverphonic)
* [Viola Enluarada](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/viola-enluarada.mscx)
* [Violão Vadio](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/violao-vadio.mscx)
* [Violets For Your Furs](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/violets-for-your-furs.mscx)
* [Viper's Dream](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/viper-s-dream.mscx) (Les Paul)
* [Viper's Dream](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/viper-s-dream2.mscx) (Django Reinhardt)
* [Virgin Mary had a Baby Boy](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/virgin-mary-had-a-baby-boy.mscx) (traditional)
* [Virgin Mary](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/virgin-mary.mscx) (West Indies)
* [Virgo](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/virgo.mscx) (Wayne Shorter)
* [Visa från Utanmyra](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/visa-fran-utanmyra.mscx)
* [Visit To Panama](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/visit-to-panama.mscx)
* [Vissi d'arte, vissi d'amore Tosca's Prayer](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vissi-d-arte-vissi-d-amore-tosca-s-prayer.mscx) (G. Puccini)
* [Vitoriosa](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vitoriosa.mscx)
* [Viva la Vida](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/viva-la-vida.mscx) (Chris Martin, Guy Berryman, Jon Buckland, Will Champion)
* [Vivo Sonhando [Dreamer]](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vivo-sonhando-dreamer.mscx) (Antonio Carlos Jobim)
* [Voce E Linda](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/voce-e-linda.mscx)
* [Você](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/voce.mscx) (Roberto Menescal
Ronaldo Bôscoli)
* [Vodka](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vodka.mscx) (Mal Waldron)
* [Vörös bor nem drága](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/voeroes-bor-nem-draga.mscx) (Hungarian folk song)
* [Vol Vistu Gaily Star](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vol-vistu-gaily-star.mscx) (Slim Gaillard)
* [Volare [Nel Blu Dipinto Di Blu]](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/volare-nel-blu-dipinto-di-blu.mscx) (Domenico Modugno)
* [Volare](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/volare.mscx)
* [Volver](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/volver.mscx) (Carlos Gardel
Transcription: Alberto Betancourt)
* [Vom Himmel Hoch](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vom-himmel-hoch.mscx) (Martin Luther & J.S. Bach)
* [Vou Te Contar [Wave]](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/vou-te-contar-wave.mscx) (Antonio Carlos Jobim)
* [Voyage](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/voyage.mscx)
* [Voyage](https://bitbucket.org/davklein/musescore-leadsheet-collection/raw/master/v/voyage2.mscx) (Paco Charlery)
